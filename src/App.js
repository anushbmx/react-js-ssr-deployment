import React, { Component } from 'react';
import Helmet from 'react-helmet';

import logo from './logo.svg';
import './App.css';
import './css/demo.css';

class App extends Component {
  render() {
    return (
      <div id="container">
      <Helmet>
          <title> Server Side Render ReactJS Application | Tech Stream</title>

          <meta name="description" content="--Description--"/>

          <meta property="og:title" content="Server Side Render ReactJS Application" />
          <meta property="og:image" content="https://techstream.org/images/img/Server-Sider-Render-React-JS-Application.jpg" />
          <meta property="og:site_name" content="Tech Stream" />
          <meta property="og:description" content="Server Side Rendering  or SSR  means you pre-render the markup of a front-end framework on a back-end system before delivering to users. A guide to do Static Pre-render of reactjs application."/>

          <meta property="og:type" content="article"/>

          <meta name="twitter:title" content="Server Side Render ReactJS Application" />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:image" content="https://techstream.org/images/img/Server-Sider-Render-React-JS-Application.jpg" />
          <meta name="twitter:site" content="@techstream_org" />
          <meta name="twitter:creator" content="@techstream_org" />
          <meta name="twitter:domain" content="techstream.org" />
          <meta name="twitter:description" content="Server Side Rendering  or SSR  means you pre-render the markup of a front-end framework on a back-end system before delivering to users. A guide to do Static Pre-render of reactjs application."/>
        </Helmet>


        <section id="header">
          <div className="row">
            <div className="column medium-4 small-12">
              <div className="row">
                <div className="column small-4 medium-3 large-2 small-centered medium-uncentered logo">
                  <a href="http://demo.techstream.org/Server-Sider-Render-React-JS-Application/">&npbsp;</a>
                </div>
              </div>
            </div>
            <div className="column medium-2 small-12">
              <div className="row">
                <div className="column small-4 medium-3 large-12 small-centered medium-uncentered nospace push-2-top ">
                  <a href="https://techstream.org/Web-Development/Server-Sider-Render-React-JS-Application" className="button"> Back to Demo</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <script async type="text/javascript" src="//cdn.carbonads.com/carbon.js?serve=CKYI5KJJ&placement=techstreamorg" id="_carbonads_js"></script>
        <section id="content">
          <div className="row">
            <div className="small-12 push-4-top column text-center">
              <img src={logo} alt="logo" className={'react-logo'} />
              <h3>Server Side Render ReactJS Application</h3>
              <p>This is a server side rendered ReactJS application, check the page source to see the pre-rendered html.</p>
            </div>
          </div>
        </section>
        <section id="footer">
        </section>
      </div>
    );
  }
}

export default App;
